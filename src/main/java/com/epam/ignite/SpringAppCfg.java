package com.epam.ignite;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.springdata.repository.config.EnableIgniteRepositories;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.epam.ignite.dto.Person;

@Configuration
@EnableIgniteRepositories
public class SpringAppCfg {

    @Bean
    public Ignite igniteInstance() {
        IgniteConfiguration cfg = new IgniteConfiguration();
        cfg.setIgniteInstanceName("springDataNode");
        cfg.setPeerClassLoadingEnabled(true);
        CacheConfiguration ccfg = new CacheConfiguration("PersonCache");
        ccfg.setIndexedTypes(Long.class, Person.class);
        cfg.setCacheConfiguration(ccfg);
        return Ignition.start(cfg);
    }
}