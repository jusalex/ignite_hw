package com.epam.ignite;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.epam.ignite.dao.PersonRepository;
import com.epam.ignite.dto.Person;

import javax.cache.Cache;

public class App {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();

        ctx.register(SpringAppCfg.class);
        ctx.refresh();
        PersonRepository repo = ctx.getBean(PersonRepository.class);

        Map<Long, Person> persons = getPersonsForSave();

// Adding data into the repository.
        repo.save(persons);

        List<Person> johns = repo.findByName("John");

        for (Person person : johns) {
            System.out.println("Found person: " + person);
        }
        Cache.Entry<Long, Person> topPerson = repo.findTopByLastNameLike("Smith");
        System.out.println("Person with surname 'Smith': " + topPerson.getValue());
    }

    @NotNull
    private static Map<Long, Person> getPersonsForSave() {
        Map<Long, Person> persons = new TreeMap<>();
        persons.put(1L, new Person(1L, "John", "Smith"));
        persons.put(2L, new Person(1L, "Brad", "Pitt"));
        persons.put(3L, new Person(1L, "Mark", "Tomson"));
        return persons;
    }
}
