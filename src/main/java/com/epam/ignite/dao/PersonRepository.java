package com.epam.ignite.dao;

import java.util.List;

import org.apache.ignite.springdata.repository.IgniteRepository;
import org.apache.ignite.springdata.repository.config.RepositoryConfig;

import com.epam.ignite.dto.Person;

import javax.cache.Cache;

@RepositoryConfig(cacheName = "PersonCache")
public interface PersonRepository extends IgniteRepository<Person, Long> {

    List<Person> findByName(String name);

    Cache.Entry<Long, Person> findTopByLastNameLike(String name);
}